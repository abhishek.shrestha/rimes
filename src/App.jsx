import { Button, Checkbox, Form, Input, DatePicker, Select } from "antd";

function App() {
  return (
    <>
      <div id="main">
        <section className="forecast-data-section">
          <div className="info-wrapper">
            <div className="info-holder">
              <div className="info-content">
                <h1 className="h2">
                  Multi-model Ensemble Climate Prediction For Thailand Domain
                </h1>
                <p className="info-note">
                  Note: Please select forecast date carefully !
                </p>
              </div>
              <div className="pattern-1">
                <img
                  src="https://res.cloudinary.com/ddkshvtyr/image/upload/v1714129029/gpaudyal/circle_7c9e07a92b.svg"
                  alt="image desc"
                />
              </div>
              <div className="pattern-2">
                <img
                  src=" https://res.cloudinary.com/ddkshvtyr/image/upload/v1714129028/gpaudyal/bubble_63672027f7.png "
                  alt="image desc"
                />
              </div>
            </div>
            <div className="form-holder">
              <div className="container">
                <h2 className="h4 form-heading">
                  Please Fill the Form Below, to generate
                  <span>
                    {" "}
                    forecast data{" "}
                    <img
                      src="https://res.cloudinary.com/ddkshvtyr/image/upload/v1714129029/gpaudyal/file_a88275dee3.svg"
                      alt="image desc"
                    />
                  </span>
                </h2>

                <Form layout="vertical">
                  <div className="form-row forecast-row">
                    <div className="form-col">
                      <h3 className="h6 form-title">Select Forecast Year</h3>
                      <div className="form-box">
                        <div className="form-box-wrap">
                          <div className="flexible">
                            <div className="form-group ">
                              <span className="input-label">Parameter</span>
                              <Select
                                defaultValue="Precipitation"
                                options={[
                                  {
                                    value: "Precipitation",
                                    label: "Precipitation",
                                  },
                                  {
                                    value: "Dropdown 2",
                                    label: "Dropdown 2",
                                  },
                                  {
                                    value: "Dropdown 3",
                                    label: "Dropdown 3",
                                  },
                                  {
                                    value: "Dropdown 4",
                                    label: "Dropdown 4",
                                  },
                                  {
                                    value: "Dropdown 5",
                                    label: "Dropdown 5",
                                  },
                                ]}
                              />
                            </div>
                          </div>
                          <div className="fixed-w">
                            <div className="form-group ">
                              <span className="input-label">Forecast Date</span>
                              {/* <RangePicker picker="month" /> */}
                              <DatePicker.RangePicker
                                picker="month"
                                format="MMM YYYY"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="form-col">
                      <h3 className="h6 form-title">Select Forecast Method</h3>
                      <div className="form-box">
                        <div className="checkbox-wrap">
                          <Checkbox>Simple Mean Method (SMM)</Checkbox>
                        </div>
                        <div className="checkbox-wrap">
                          <Checkbox>
                            Skilled Weighted Average Method (WAM)
                          </Checkbox>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-col w-full">
                      <h3 className="h6 form-title">Select Model Datasets</h3>
                      <div className="form-box modal-box">
                        <div className="checkbox-wrap-inline">
                          <Checkbox>ECMWF</Checkbox>
                        </div>
                        <div className="checkbox-wrap-inline">
                          <Checkbox>CanSIPS-IC3</Checkbox>
                        </div>
                        <div className="checkbox-wrap-inline">
                          <Checkbox>NASA-GEOSSS</Checkbox>
                        </div>
                        <div className="checkbox-wrap-inline">
                          <Checkbox>NCAR-RSMAS-CCSM4</Checkbox>
                        </div>
                        <div className="checkbox-wrap-inline">
                          <Checkbox>UKMO</Checkbox>
                        </div>
                        <div className="checkbox-wrap-inline">
                          <Checkbox>Meteo-France</Checkbox>
                        </div>
                        <div className="checkbox-wrap-inline">
                          <Checkbox>DWD</Checkbox>
                        </div>
                        <div className="checkbox-wrap-inline">
                          <Checkbox>CMCC</Checkbox>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-col">
                      <h3 className="h6 form-title">
                        Select Predictand Dataset
                      </h3>
                      <div className="form-box height-auto">
                        <div className="form-group">
                          <Select
                            defaultValue="ERA5"
                            options={[
                              {
                                value: "ERA5",
                                label: "ERA5",
                              },
                              {
                                value: "Dropdown 2",
                                label: "Dropdown 2",
                              },
                              {
                                value: "Dropdown 3",
                                label: "Dropdown 3",
                              },
                              {
                                value: "Dropdown 4",
                                label: "Dropdown 4",
                              },
                              {
                                value: "Dropdown 5",
                                label: "Dropdown 5",
                              },
                            ]}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="form-col">
                      <h3 className="h6 form-title">Select Hindcast Period</h3>
                      <div className="form-box height-auto">
                        <DatePicker.RangePicker
                          picker="month"
                          format="MMM YYYY"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="cta-holder">
                    <Button type="primary">Check Models</Button>
                    <Button>Clear</Button>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}

export default App;
