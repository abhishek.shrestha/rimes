import React from "react";
import ReactDOM from "react-dom/client";
import { ConfigProvider } from "antd";

import App from "./App.jsx";
import "./styles/main.less";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: "#0575E6",
        },
      }}
    >
      <App />
    </ConfigProvider>
  </React.StrictMode>
);
